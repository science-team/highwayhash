/* Sanity test code for Debian's Highwayhash package [autopkgtest]
 * Author: Adam Borowski
 * Modified: Zhou Mo
 */
#include <assert.h>
#include <highwayhash/c_bindings.h>

static const uint64_t shkey[2]={0xdeadbeef,0xcafebabe};
static const HHKey hhkey={3,14,15,926}; // 4×uint64_t

int main()
{
    assert((uint64_t) 0xaf0a25067c014659 == SipHashC(shkey, "meow", 4));
    assert((uint64_t) 0x600708416bfbe7ad == SipHash13C(shkey, "meow", 4));
	assert((uint64_t) 0x01aeb7e482f04c46 == HighwayHash64(hhkey, "meow", 4));
    return 0;
}
